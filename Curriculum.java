import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/* ToDo
    - Add Validation System for some methods
    - Add Documentation
    - Provide Algorithms
    - Make Custom Exception/s (not sure yet)

 */
/**
 * Curriculum Reference Class
 * @author
 */
public class Curriculum {
    protected String name;
    protected String program;
    protected ArrayList<AbstractCourse> courses;
    private Scanner scan;
    private final AvailablePrograms[] PROGRAMS = AvailablePrograms.values();
    private final String FILE_NAME;
    private final String DATA_FILE_LOCATION = "Data\\"; // courses Storage
    private final String COURSE_CURRICULUM_REFERENCE_LOCATION = "Courses\\"; // courses Storage

    // incomplete - make a system to check a
    public Curriculum(String name, String program) throws Exception {
        this.name = name;
        if (AvailablePrograms.checkIfProgramIsAvailable(program)){
            this.program = program;
        } else {
            throw new Exception();
        }
        FILE_NAME = name.toLowerCase()+ program.toLowerCase()+".txt";
        checkFile();
    }

    /* Algorithm
        - Declare Scanner Class
        - Surround it in Try Catch
        - instantiate scanner and check if scanner will make an Exception
        - if it will make an exception, call the makeNewFile Method
        - call the method to populate the course ArrayList
     */
    private void checkFile() {
        try {
            scan = new Scanner(new File(DATA_FILE_LOCATION + FILE_NAME));
        } catch (FileNotFoundException e){
            makeNewFile();
        } finally {
            if (scan != null) scan.close();
            assignSubjects();
        }
    }

    /* Algorithm
        - Declare FileWriter and Scanner Class
        - Surround it with Try Catch
        - Instantiate Scanner and FileWriter
        - Scan the Course Curriculum File and get data from each line
        - write the scanned line plus the grade, taking, and a newline in a new file
    */
    private void makeNewFile() {
        FileWriter writer;
        try {
            scan = new Scanner(new File(COURSE_CURRICULUM_REFERENCE_LOCATION + program.toLowerCase()+".txt"));
            writer = new FileWriter(DATA_FILE_LOCATION + FILE_NAME);
            while (scan.hasNextLine()) {
                writer.write(scan.nextLine()+",0"+",false"+"\n");
            }
            scan.close();
            writer.close();
        } catch (NumberFormatException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* Algorithm
        - Instantiate ArrayList, and set size to the number of lines in the data file
        - Instantiate Scanner for File Reading
        - Surround with Try Catch
        - Read each line in the data file and parse it into a String Array
        - if else statement to check what type of Course is the read line
        - create Object and add it to the ArrayList
        - close the Scanner
    */
    private void assignSubjects() {
        courses = new ArrayList<>(countFileLines(program));
        try {
            scan = new Scanner(new File(DATA_FILE_LOCATION + FILE_NAME));
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                String[] temp = line.split(",");
                float tempFloat = Float.parseFloat(temp[5]);
                if (temp[0].equals("E")) {
                    courses.add(new Elective(temp[0].charAt(0), temp[1].charAt(0), temp[2], temp[3],
                            Float.parseFloat(temp[4]), tempFloat, Boolean.parseBoolean(temp[6])));
                } else if (temp[6].equals("false") && (tempFloat >= 60)) {
                    courses.add(new CourseTaken(Byte.parseByte(temp[0]), Byte.parseByte(temp[1]), temp[2],
                            temp[3], Float.parseFloat(temp[4]), tempFloat, false, temp[6]));
                } else {
                    courses.add(new Course(Byte.parseByte(temp[0]),Byte.parseByte(temp[1]),temp[2],
                            temp[3], Float.parseFloat(temp[4]), Float.parseFloat(temp[5]),
                            Boolean.parseBoolean(temp[6])));
                }
            }
            scan.close();
        } catch (NumberFormatException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* Algorithm
        - initialize int named numberOfLines
        - Declare and Instantiate Scanner
        - Surround with Try Catch
        - use Scanner to go through all lines of the file and increment numberOfLines until it finds no more lines
        - close Scanner
        - return numberOfLines
     */

    private int countFileLines(String program) {
        int lines = 0;
        try {
            scan = new Scanner(new File(COURSE_CURRICULUM_REFERENCE_LOCATION + program.toLowerCase()+".txt"));
            while (scan.hasNextLine()) {
                lines++;
                scan.nextLine();
            }
            scan.close();
        } catch (NumberFormatException | FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    // Temporary - For testing only
    public void printCurriculumWithGrades() {
        byte sem = 0;
        boolean print = false;
        System.out.println("Name: "+ name);
        System.out.println("Program: " + program);
        for (AbstractCourse c : courses) {
            if (!(c instanceof Elective)) {
                if (sem != c.getSemester()) {
                    sem = c.getSemester();
                    continueKey();
                    System.out.printf("%-5s%s%-14d%s%-100d%5s%n", "", "Year: ", c.getYear(), "Semester: ", c.getSemester(), "");
                    System.out.printf("%-5s%-20s%-85s%-15s%-15s%-20s%5s%n", "", "Course Number", "Course", "Units", "Grade", "Remarks", "");
                    System.out.printf("%-5s%-20s%-85s%-15s%-15s%-20s%5s%n", "", "---------------", "--------------------------------" +
                            "-------------------------------------------", "-------", "-------", "-------------", "");
                }
                if (c.getGrade() < 60) {
                    System.out.printf("%-5s%-20s%-85s%-15s%-15s%-20s%5s%n", "", c.getCourseNo(), c.getCourseName(), c.getUnit(), "", c.getRemarks(), "");
                } else {
                    System.out.printf("%-5s%-20s%-85s%-15s%-15s%-20s%5s%n", "", c.getCourseNo(), c.getCourseName(), c.getUnit(), c.getGrade(), c.getRemarks(), "");
                }
            } else {
                if (!print) {
                    continueKey();
                    System.out.printf("%-5s%s%n", "", "Electives");
                    System.out.printf("%-5s%-20s%-85s%-30s%-20s%5s%n", "", "Course Number", "Course", "Units", "Remarks", "");
                    System.out.printf("%-5s%-20s%-85s%-30s%-20s%5s%n", "", "---------------", "--------------------------------" +
                            "-------------------------------------------", "-------------------", "-----------------------", "");
                    print = true;
                }
                System.out.printf("%-5s%-20s%-85s%-30s%-20s%5s%n", "", c.getCourseNo(), c.getCourseName(), c.getUnit(), c.getRemarks(), "");
            }
        }
        scan.close();
    }

    // temporary - for testing only
    private void continueKey() {
        System.out.println("\nPress [Enter] to Show Next Semester");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    // Search Variables and Methods
    private final Search getIndexOfCourseNo = s1 -> search(s1,AbstractCourse::getCourseNo);
    private final Search getIndexOfCourseName = s1 -> search(s1,AbstractCourse::getCourseName);
    private final Search getIndexOfYearWithSemester = s1 -> search(s1,AbstractCourse::getYear,AbstractCourse::getSemester);

    /* Algorithm
        - Use an appropriate search algorithm for Unsorted List
        - Pass on a String Value to find its index in the data file
        - return the index
     */
    private int search(String searchValue1, CourseProperty<String> searchProperty2) {
        for (int i = 0; i < courses.size(); i++)
            if (searchValue1.equals(searchProperty2.getCourse(courses.get(i)))) return i;
        return -1;
    }

    /* Algorithm
        - Same as the previous algorithm except the search order starts in the last index to the first index
     */
    private int search(String searchValue1, CourseProperty<Byte> searchProperty1, CourseProperty<Byte> searchProperty2) {
        String s;
        for (int i = courses.size()-1; i > 0; i--) {
            s = searchProperty1.getCourse(courses.get(i))+","+searchProperty2.getCourse(courses.get(i));
            if (searchValue1.equals(s)) return i;
        }
        return -1;
    }

    // incomplete
    public void selectElective(String courseName, String electiveCourseNo) throws IllegalAccessException {
        int courseToBeChangeIndex = getIndexOfCourseName.find(courseName);
        int electiveIndex = getIndexOfCourseNo.find(electiveCourseNo);
        char electiveNum = courseName.replaceAll("\\D", "").charAt(0);

        if ((byte) '0' == courses.get(electiveIndex).getSemester() || ((byte) electiveNum == courses.get(electiveIndex).getSemester())) {
            courses.get(courseToBeChangeIndex).setCourseNo(courses.get(electiveIndex).getCourseNo());
            courses.get(courseToBeChangeIndex).setCourseName(courses.get(electiveIndex).getCourseName());
            if (courses.get(electiveIndex).getGrade() > 0) {
                courses.add(courseToBeChangeIndex+1, new Course(courses.get(courseToBeChangeIndex).getYear(),
                        courses.get(courseToBeChangeIndex).getSemester(),courses.get(courseToBeChangeIndex)
                        .getCourseNo()+"L", courses.get(courseToBeChangeIndex).getCourseName()+" (Lab)",
                        courses.get(electiveIndex).getGrade(), 0.0f, false));
            }
            courses.get(electiveIndex).setSemester((byte) electiveNum);
            courses.get(electiveIndex).setTaking(true);
        } else {
            throw new IllegalAccessException("Elective Number should be equal to the Elective Number in the Course");
        }
    }

    /* Algorithm

     */
    public void editCourse(String courseNoToEdit, String newCourseNo) {
        int i = getIndexOfCourseNo.find(courseNoToEdit);
        String[] temp;
        try {
            temp = searchCourseDetail(newCourseNo);
            courses.get(i).setCourseNo(temp[0]);
            courses.get(i).setCourseName(temp[1]);
            courses.get(i).setUnit(Float.parseFloat(temp[2]));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void editGrade(String courseNo, float newGrade) {
        if (newGrade > 99.99 || newGrade < 60.0) {
            throw new ArithmeticException("Grade should be in between 60.0 and 99.99");
        } else {
            int index = getIndexOfCourseNo.find(courseNo);
            courses.get(index).setGrade(newGrade);

            CourseTaken temp = new CourseTaken(courses.get(index).getYear(), courses.get(index).getSemester(),
                    courses.get(index).getCourseNo(), courses.get(index).courseName,
                    courses.get(index).getUnit(), courses.get(index).getGrade(),
                    courses.get(index).isTaking(), program.toLowerCase());

            courses.set(index, temp);
        }
    }

    public void switchTaking(String courseNo) {
        int index = getIndexOfCourseNo.find(courseNo);
        courses.get(index).setTaking(!courses.get(index).isTaking());
    }

    // Incomplete - Make Custom Exception
    public void addCourse(byte year, byte semester, String courseNo) throws Exception {
        if (getIndexOfCourseNo.find(courseNo) < 0) {
            int index = getIndexOfYearWithSemester.find(year+","+semester);
            String[] newCourse = searchCourseDetail(courseNo);
            courses.add(index+1,new Course(year,semester,courseNo,newCourse[1],Float.parseFloat(newCourse[2]),
                    0.0f,false));
        } else {
            throw new Exception("Course Already Exist in the Curriculum");
        }
    }

    // incomplete - need to gather ideas for how a irregular/shifter students curriculum look like
    public void shiftProgram(String program) {
        String newName = program.toLowerCase()+".txt";
        try {
            FileWriter writer = new FileWriter(DATA_FILE_LOCATION+FILE_NAME);
            scan = new Scanner(new File(DATA_FILE_LOCATION+newName));
            for (AbstractCourse c : courses) {
                if (!(c instanceof CourseTaken)) {
                    String[] temp = scan.nextLine().split(",");

                } else {
                    writer.write(c.toString());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveFile() {
        try (FileWriter fileWriter = new FileWriter(DATA_FILE_LOCATION+FILE_NAME)) {
            for (AbstractCourse c : courses)
                fileWriter.write(c.toString()+"\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void reload() {
        assignSubjects();
    }

    private String[] searchCourseDetail(String courseNo) throws Exception {
        for (AvailablePrograms availablePrograms : PROGRAMS) {
            try {
                scan = new Scanner(new File(COURSE_CURRICULUM_REFERENCE_LOCATION + availablePrograms.toString().toLowerCase() + ".txt"));
                while (scan.hasNextLine()) {
                    String line = scan.nextLine();
                    if (line.contains(courseNo)) {
                        scan.close();
                        return line.substring(4).split(",");
                    }
                }
            } catch (FileNotFoundException ignored) {
            }
            scan.close();
        }
        throw new Exception();
    }

}
