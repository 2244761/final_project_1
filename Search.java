/**
 * A functional interface for performing search operations on a string. This interface is intended
 * to provide a single abstract method that encapsulates a search strategy, such as finding a
 * character or substring, or any other form of search operation that returns an integer index or
 * value based on a string input.
 *
 * <p>The {@code find} method is designed to accept a string as input and return an integer
 * representing the result of a search operation. This result could be the index of a particular
 * character or substring, or any other value that represents an outcome of the search strategy.
 *
 * <p>This interface is typically used to implement lambda expressions or method references that
 * encapsulate different search behaviors with a focus on flexibility and reusability of code.
 *
 * <p>Example usage:
 * <pre>{@code
 * Search firstCharSearch = s -> s.indexOf("A");
 * int index = firstCharSearch.find("Apple");
 * System.out.println("Index of 'A' in 'Apple': " + index);
 * }</pre>
 *
 * @apiNote This interface is a {@link FunctionalInterface} and is expected to be used primarily
 *          with lambda expressions or method references.
 * @author
 */
@FunctionalInterface
public interface Search {
    /**
     * Performs a search operation on the provided string and returns an integer result.
     *
     * @param s1 the string to be searched.
     * @return an integer indicating the result of the search operation. The meaning of this result
     *         depends on the specific search implementation (e.g., index of a character, occurrence count).
     */
    int find(String s1);
}
