/* ToDo
    - Finalize Design/Layout
    - Add Documentation
 */
/**
 * AbstractCourse Class
 * @author
 */
public abstract class AbstractCourse implements Comparable<AbstractCourse> {
    protected byte year;
    protected byte semester;
    protected String courseName;
    protected String courseNo;
    protected float unit;
    protected float grade;
    protected boolean taking;

    /**
     * Template in file: Year,Semester,Course No,Course Name,Unit,Grade,Taking
     * Example: 3,2,CS 322L,Data Science (Lab),1
     * @param year
     * @param semester
     * @param courseNo
     * @param courseName
     * @param unit
     * @param grade
     * @param taking
     */
    protected AbstractCourse(byte year, byte semester, String courseNo, String courseName, float unit, float grade, boolean taking) {
        this.year = year;
        this.semester = semester;
        this.courseNo = courseNo;
        this.courseName = courseName;
        this.unit = unit;
        this.grade = grade;
        this.taking = taking;
    }

    public void setGrade(float grade) {
        if (grade != 0 && !(grade > 60)) taking = false;
        this.grade = grade;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setSemester(byte semester) {
        this.semester = semester;
    }

    public void setUnit(float unit) {
        this.unit = unit;
    }

    public void setYear(byte year) {
        this.year = year;
    }

    public void setTaking(boolean taking) {
        this.taking = taking;
    }

    public float getUnit() {
        return unit;
    }

    public String getCourseName() {
        return courseName;
    }

    public float getGrade() {
        return grade;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public boolean isTaking() {
        return taking;
    }

    public byte getSemester() {
        return semester;
    }

    public byte getYear() {
        return year;
    }

    public abstract String getRemarks();
    public abstract void editDetails(); // temporary maybe? // probably to be used to simplify setting the Course Details
    public abstract String toString();
}
