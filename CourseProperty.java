/**
 * A Functional Interface designed for abstracting methods that retrieve properties from instances
 * of {@link AbstractCourse}. This interface is used to enable higher-order functions that can
 * operate on various properties of {@code AbstractCourse} instances without tying down to specific
 * property methods.
 *
 * <p>For example, this interface can be used to reference any getter method from {@code AbstractCourse}
 * that returns a type {@code T}. It allows for writing generic code that works with different properties
 * of courses, such as name, id, or any custom attributes added in subclasses of {@code AbstractCourse}.
 *
 * @param <T> the type of the property that the method reference will return. This type parameter
 *            allows the interface to be used with methods that return any type of data, enhancing
 *            its flexibility and reusability.
 *
 * @author
 * @see AbstractCourse
 */
@FunctionalInterface
public interface CourseProperty<T> {
    /**
     * Gets a property from an instance of {@code AbstractCourse}.
     *
     * @param course the {@code AbstractCourse} instance from which to retrieve the property.
     * @return the property of type {@code T}.
     */
    T getCourse(AbstractCourse course);
}
