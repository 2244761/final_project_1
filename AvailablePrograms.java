/**
 * Enumerations representing the available programs in the project. This enum provides a standardized
 * list of program abbreviations corresponding to the courses offered. Each constant in this enum
 * represents a specific program available for enrollment or reference within the system.
 *
 * <p>Use the {@link #checkIfProgramIsAvailable(String)} method to dynamically verify if a given
 * program is listed among the available ones, thereby assisting in validation checks against user input
 * or external data sources.
 *
 * <p>When adding new programs to the project, simply add new constants to this enum with the appropriate
 * program abbreviation. Ensure that abbreviations are unique and represent the program succinctly.
 *
 * <p>Current list of programs:
 * <ul>
 *   <li>{@code BSCS} - Bachelor of Science in Computer Science</li>
 *   <li>{@code BSIT} - Bachelor of Science in Information Technology</li>
 * </ul>
 *
 * @author
 */
enum AvailablePrograms {
    /** Bachelor of Science in Computer Science */
    BSCS,
    /** Bachelor of Science in Information Technology */
    BSIT;

    /**
     * Checks if a specified program abbreviation exists among the available programs.
     * This method is case-insensitive, converting the provided string to uppercase before
     * comparison.
     *
     * @param program the program abbreviation to check, e.g., "BSCS" or "bsit".
     * @return {@code true} if the program is available, {@code false} otherwise.
     */
    public static boolean checkIfProgramIsAvailable(String program) {
        String s1 = program.toUpperCase();
        AvailablePrograms[] s2 = AvailablePrograms.values();
        for (AvailablePrograms p : s2) {
            if (p.toString().equals(s1)) {
                return true;
            }
        }
        return false;
    }
}
