import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class test {
    public static void main(String[] args) throws Exception {
        Curriculum curriculum = new Curriculum("karl", "BSCS");
        curriculum.printCurriculumWithGrades();

        curriculum.editGrade("CS 122", 89.44f);

//        try {
//            curriculum.addCourse((byte) 1, (byte) 1,"IT 111");
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
        try {
            curriculum.selectElective("Elective 1", "CSE 15");
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        curriculum.printCurriculumWithGrades();
        curriculum.saveFile();

//        ArrayList<String> arr = new ArrayList<>(4);
//        arr.add("Hello");
//        arr.add("World");
//        arr.add("Java");
//        System.out.println(arr);
//        arr.add(3,"Programming");
//        System.out.println(arr);

//        JFrame frame = new JFrame("Simple JTable Example");
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//
//        String[] columnNames = {"Course No.", "Course Name", "Units", "Grade"};
//        Object[][] data = {
//                {"CS 122", "Computer Programming 2", 2, 99.9},
//                {"CS 122L", "Computer Programming 2 (Lab)", 1, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//                {"CS 121", "Digital Logic Design", 3, 99.9},
//
//        };
//
//        System.out.println(data[0][1]);
//        JTable table = new JTable(data, columnNames);
//        // Change specific row height and column width
//        table.setRowHeight(1, 20); // Increases the height of the second row
//        table.getColumnModel().getColumn(1).setPreferredWidth(450);
//        table.getColumnModel().getColumn(0).setPreferredWidth(25);
//        table.getColumnModel().getColumn(2).setPreferredWidth(10);
//        table.getColumnModel().getColumn(3).setPreferredWidth(10);// Sets the width of the second column
//
//        JScrollPane scrollPane = new JScrollPane(table);
//        table.setFillsViewportHeight(true);
//
//        frame.add(scrollPane, BorderLayout.CENTER);
//        frame.setSize(750, 500);
//        frame.setVisible(true);
//
    }
}
