/* ToDo
    - Finalize Design/Layout
    - Add Documentation
 */
/**
 * Course Reference Class
 * @author
 */
public class Course extends AbstractCourse {
    /**
     * Template in file: Year,Semester,Course No,Course Name,Unit,Grade,Taking
     * Example: 3,2,CS 322L,Data Science (Lab),1
     * @param year
     * @param semester
     * @param courseNo
     * @param courseName
     * @param unit
     * @param grade
     * @param taking
     */
    public Course(byte year, byte semester, String courseNo, String courseName, float unit, float grade, boolean taking) {
        super(year, semester, courseNo, courseName, unit, grade, taking);
    }

    @Override
    public String getRemarks() {
        if (!taking && year == 1 && semester == 1) {
            taking = true;
            return "In Progress";
        }
        else if (courseName.toLowerCase().contains("elective") && taking) return "Not Selected";
        else if (taking) return "In Progress";
        else return "Not Taken";
    }

    @Override
    public float getUnit() {
        return unit;
    }

    @Override
    public void editDetails() {

    }

    @Override
    public int compareTo(AbstractCourse o) {
        return 0;
    }

    @Override
    public String toString() {
        return year+","+semester+","+courseNo+","+courseName+","+unit+","+grade+","+taking;
    }
}
