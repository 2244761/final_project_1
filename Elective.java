/* ToDo
    - Finalize Design/Layout
    - Add Documentation
 */

/**
 * Elective Reference Class
 * @author
 */
public class Elective extends AbstractCourse{
    /**
     * Year = Elective Indicator
     * Semester = Which Elective Number is it assigned to
     * CourseNo, CourseName, and Unit is the same
     * Grade = How many Lab Units
     * Taking = If the Elective has Been Selected
     *
     * Template in file: Elective Indicator,Elective Number,Course No.,Course Name,Unit1,Unit2,Selected
     * Example: E,0,CSE 18,Machine Learning,3,0
     * @param elective
     * @param electiveNum
     * @param courseNo
     * @param courseName
     * @param unit1
     * @param unit2
     * @param selected
     */
    protected Elective(char elective, char electiveNum, String courseNo, String courseName,
                       float unit1, float unit2, boolean selected) {
        super((byte) elective,(byte) electiveNum, courseNo, courseName, unit1, unit2, selected);
    }

    @Override
    public String getRemarks() {
        char electiveNum = (char)semester;
        if (electiveNum != '0' && !taking) return  "For Elective " + electiveNum;
        else if (taking) return "Assigned to Elective "+electiveNum;
        else return "";
    }

    @Override
    public float getUnit() {
        return unit;
    }

    @Override
    public void editDetails() {

    }

    @Override
    public String toString() {
        String unit = (grade <= 0) ? this.unit+",0" : this.unit+","+grade;
        return (char)year+","+(char)semester+","+courseNo+","+courseName+","+unit+","+taking;
    }

    @Override
    public int compareTo(AbstractCourse o) {
        return 0;
    }
}
