/* ToDo
    - Finalize Design/Layout
    - Add Documentation
 */
/**
 * CourseTaken Reference Class
 * @author
 */
public class CourseTaken extends AbstractCourse {
    private final boolean FAILED;
    private String program;

    /**
     * Template in file: Year,Semester,Course No,Course Name,Unit,Grade,Taking
     * Example: 3,2,CS 322L,Data Science (Lab),1
     * @param year
     * @param semester
     * @param courseNo
     * @param courseName
     * @param unit
     * @param grade
     * @param taking
     */
    protected CourseTaken(byte year, byte semester, String courseNo, String courseName, float unit, float grade,
                          boolean taking, String program) {
        super(year, semester, courseNo, courseName, unit, grade, taking);
        this.program = program;
        FAILED = this.grade < 75 && this.grade > 60;
    }

    protected void setProgram(String program) {
        this.program = program;
    }

    @Override
    public String getRemarks() {
        return (FAILED) ? "Failed" : "Passed";
    }

    @Override
    public float getUnit() {
        return unit;
    }

    @Override
    public void editDetails() {

    }

    @Override
    public String toString() {
        return year+","+semester+","+courseNo+","+courseName+","+unit+","+grade+","+taking+","+program;
    }

    @Override
    public int compareTo(AbstractCourse o) {
        return (o.getCourseNo().equals(this.courseNo)) ? 0 : 1;
    }
}
